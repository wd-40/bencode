# bencode #

bencode - bencode parser/deserializer and serializer written in D

# nice #

	import bencode;
	
	auto file = cast(ubyte[])read("OnlyHuman-e-0.21.7-20170411.torrent");

	auto bencode = bencodeParse(file);
	assert(equal(*bencode["comment"].str(), "OnlyHuman-e-0.21.7-20170411"));
	assert(equal(*bencode["info"]["files"][6]["path"][0].str(), "OnlyHuman-e-0.21.7-20170411.txt"));

	bencode["info"]["files"] ~= "test";
	bencode["info"]["files"] ~= [ "test2", "test3" ];
	bencode["info"]["files"] ~= [ "test4": new BElement(1000), "test5": new BElement([ "very nice" ]) ];
#
	import bencode;
	
	bencode = new BElement(BDict.init);

	bencode["length"] ~= 1000;
	bencode["path"] ~= [ "very": "nicep" ];
	bencode["path"] ~= [ "very good": "nice" ];

	// oops typo, overwrite
	bencode["path"] ~= [ "very": "nice" ];

	assert(bencode.serialize() == "d6:lengthi1000e4:pathd4:very4:nice9:very good4:niceee");